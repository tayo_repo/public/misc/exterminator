# Exterminator - An all in one shitpost, but kinda useful bot

Developers are not responsible for your hurt feelings. Drink responsibly.

Join our [Discord](https://discord.gg/bkK5GxN) for reporting issues, or suggesting features.

## Setup

Please keep in mind that we are currently developing this at a very fast pace and this may be outdated. This also only works on Linux, but you're welcome to get it running on
other platforms.

1.  Run ./build.sh.
2.  Run ./start.sh in the output directory initially to generate or update config.
3.  Add your token and database info in config.json.
4.  The bot uses the database "Exterminator". Make sure it exists in your local database.
5.  Run the queries in the Database folder on the repo. If you don't, the bot's useless.
6.  Re-run ./start.sh in the output directory.
7.  Enjoy. Or don't. Up to you.

## Commands

This will be replaced by a wiki soonTM (probably, who even cares).

- !!ping - responds with "pong".
- !!mock - mocks the provided user with their last message.
- !!pickle - shows your pickle size. May or may not reduce it over time with use. Nobody cares how large your pickle is.

## Authors and License

The bot was developed by [Lesley van der Lee](https://gitlab.com/LesleyvdLee/)).

This software is open source, licenced under the Don't Be A Dick Public License. Please see the LICENSE file for more information.
