# Build the bot
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS core
WORKDIR /exterminator
COPY / /exterminator/
RUN rm -rf ./out && rm -rf ./obj && rm -rf ./bin
RUN dotnet publish -r linux-x64 -c Release -o out
RUN cp -r ./src/AnimeFaceDetection ./out/AnimeFaceDetection
RUN cp -r ./src/Migrations ./out/Migrations
RUN cp ./config.json ./out/config.json

# Build the python face detection runtime
FROM python:3.7-buster AS python
WORKDIR /exterminator
COPY --from=core /exterminator/out/ ./
RUN pip install numpy && pip install cython
RUN cd ./AnimeFaceDetection && make clean && make

# Build the final runtime container
FROM mcr.microsoft.com/dotnet/core/runtime:3.1 AS runtime
WORKDIR /exterminator
COPY --from=python /exterminator/ ./
ENTRYPOINT ["dotnet", "Exterminator.dll"]
