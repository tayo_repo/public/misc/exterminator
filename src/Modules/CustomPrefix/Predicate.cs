using System.Collections.Generic;
using System.Threading.Tasks;
using DSharpPlus.Entities;

using Exterminator.Modules.CustomPrefix.Queries;

namespace Exterminator.Modules.CustomPrefix {
    public abstract class Predicate {
        public static async Task<int> CustomPrefixPredicate(DiscordMessage msg) {
            List<CustomPrefixes> prefixes = await CustomPrefixes.FindByGuild(msg.Channel.GuildId.ToString());
            if (prefixes.Count > 0) {
                return msg.Content.StartsWith(prefixes[0].prefix) ? prefixes[0].prefix.Length : -1;
            } else {
                return msg.Content.StartsWith("!!") ? 2 : -1;
            }
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        public virtual void Register() {
            throw new System.NotImplementedException();
        }
    }
}