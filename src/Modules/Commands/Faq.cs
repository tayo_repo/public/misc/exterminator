using System.Collections.Generic;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

using Exterminator.Interfaces.Core;
using Exterminator.Modules.Core;

namespace Exterminator.Modules.Commands {
    public class Faq : BaseCommand {
        /* 
         * Implements the Command() function which is called when the
         * CommandsNext Module triggers it
         */
        [Command("faq")]
        [Description("DM's an embed with some answers to questions you might have.")]
        public async Task Command(CommandContext ctx) {

            var embed = new DiscordEmbedBuilder();
            embed = embed.WithColor(DiscordColor.Orange);
            embed = embed.WithAuthor("Exterminator FAQ", null, ctx.Client.CurrentUser.AvatarUrl);
            embed = embed.WithDescription("Hopefully this will help you out a bit.");


            embed = embed.AddField("Where can I get an invite?",
                "Here's a public invite for Exterminator https://discord.com/oauth2/authorize?client_id=729343804519219211&scope=bot&permissions=8");
            embed = embed.AddField("Where can I find the website for Exterminator?",
                "Right here! http://www.exterm.io/ — Note: In progress");
            embed = embed.AddField("Where can I leave my feedback?",
                "For now there's no website to leave feedback.\nJoin our Discord server here: https://discord.gg/y2Z74sy");
            embed = embed.AddField("Any way I can help with development?",
                "Join our Discord server here: https://discord.gg/y2Z74sy");

            embed = embed.WithFooter("\u00a9 TAYO Creative — Built for the lulz");

            if (ctx.Member == null) {
                await ctx.RespondAsync(null, false, embed);
            } else {
                try {
                    var dm = await ctx.Member.CreateDmChannelAsync();
                    await dm.SendMessageAsync(null, false, embed);
                    await ctx.RespondAsync($"Ight cool it's in your DM's :smirk:");
                } catch (System.Exception exception) {
                    if (exception.Message.Contains("403") == true) {
                        await ctx.RespondAsync($"Bruh I can't DM you the FAQ if you don't allow me");
                    } else {
                        throw exception;
                    }
                }
            }
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        override public void Register() {
            Program.CommandsNext.RegisterCommands<Faq>();
            Logger.Log("Set up faq command!", Logger.Level.INFO);
        }
    }
}