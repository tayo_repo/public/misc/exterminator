using System.Threading.Tasks;
using System.Linq;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

using Exterminator.Modules.Core;
using Exterminator.Modules.Core.Database.Queries;

namespace Exterminator.Modules.Commands {

    [Group("levelreward")]
    public class LevelRewards : BaseCommand {
        [Command("add")]
        [RequirePermissions(DSharpPlus.Permissions.Administrator)]
        public async Task Add(CommandContext ctx, DiscordRole role = null, int level = 0) {
            if (role == null) {
                await ctx.RespondAsync($"{ctx.Member.Mention} I need a role to add, is that so difficult?.");
                return;
            }

            if (level == 0) {
                await ExperienceRoleRewards.Delete(ctx.Guild.Id.ToString(), role.Id.ToString());
                await ctx.RespondAsync($"{ctx.Member.Mention} Removed the level reward if it was there.");
                return;
            }

            var reward = new ExperienceRoleRewards(null, ctx.Guild.Id.ToString(), role.Id.ToString(), (int)level, null);
            await ExperienceRoleRewards.Insert(reward);
            await ctx.RespondAsync($"{ctx.Member.Mention} Added the level reward for level {level}.");
        }

        [Command("remove")]
        [RequirePermissions(DSharpPlus.Permissions.Administrator)]
        public async Task Remove(CommandContext ctx, DiscordRole role = null) {
            if (role == null) {
                await ctx.RespondAsync($"{ctx.Member.Mention} I need a role to remove, is that so difficult?");
                return;
            }

            await ExperienceRoleRewards.Delete(ctx.Guild.Id.ToString(), role.Id.ToString());
            await ctx.RespondAsync($"{ctx.Member.Mention} Removed the level reward if it was there.");
        }

        [Command("list")]
        [RequirePermissions(DSharpPlus.Permissions.Administrator)]
        public async Task List(CommandContext ctx) {
            var embed = new DiscordEmbedBuilder();
            embed = embed.WithColor(DiscordColor.Orange);
            embed = embed.WithAuthor($"Level rewards for {ctx.Guild.Name}", null, ctx.Client.CurrentUser.AvatarUrl);
            embed = embed.WithFooter("\u00a9 TAYO Creative — Built for the lulz");
            embed = embed.WithDescription(
                $"These roles are given when a user reaches a **server** level.\n" +
                "These levels are only used internally for server management.");

            var roleRewards = await ExperienceRoleRewards.FindByGuild(ctx.Guild.Id.ToString());

            foreach (var role in roleRewards) {
                DiscordRole discordRole = null;

                try {
                    discordRole = ctx.Guild.Roles.First(x => x.Id.ToString() == role.roleId);
                } catch (System.Exception) {
                    await ExperienceRoleRewards.Delete(role.guildId, role.roleId);
                    continue;
                }

                if (discordRole != null) {
                    embed = embed.AddField(discordRole.Name, $"Role ID: {role.roleId}", true);
                }
            }

            await ctx.RespondAsync(null, false, embed);
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        override public void Register() {
            Program.CommandsNext.RegisterCommands<LevelRewards>();
            Logger.Log("Set up level rewards module!", Logger.Level.INFO);
        }
    }
}