using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

using Exterminator.Modules.Core;
using DSharpPlus.Entities;
using System;

namespace Exterminator.Modules.Commands {
    public class Scramble : BaseCommand {
        [Command("scramble")]
        [Description("Scrambles a message while preserving whitespace.")]
        public async Task Command(CommandContext ctx, DiscordMember member = null) {
            await ctx.TriggerTypingAsync();

            if (member == ctx.Client.CurrentUser) {
                await ctx.RespondAsync($"{ctx.Member.Mention} I'm not going to scramble my own message...");
                return;
            }

            IReadOnlyList<DiscordMessage> messages;
            List<DiscordMessage> targetMessages = new List<DiscordMessage>();
            DiscordMessage targetMessage = null;

            if (member == null) {
                messages = await ctx.Channel.GetMessagesAsync(50);
            } else {
                messages = await ctx.Channel.GetMessagesAsync(100);
                foreach (DiscordMessage msg in messages) {
                    if (msg.Author.Equals(member)) {
                        targetMessages.Add(msg);
                    }
                }
            }

            if ((member != null && targetMessages.Count == 0) || messages.Count == 0) {
                await ctx.RespondAsync($"{ctx.Member.Mention} no idea how, but you broke it, great....");
                return;
            }

            if (member == null) {
                int index = 1;
                while (messages[index].Content.StartsWith("!!") || messages[index].Author == ctx.Client.CurrentUser) {
                    index++;
                }
                targetMessage = messages[index];
            } else {
                int index = 0;
                while (targetMessages[index].Content.StartsWith("!!")) {
                    index++;
                }
                targetMessage = targetMessages[index];
            }

            if (targetMessage == null) {
                await ctx.RespondAsync($"{ctx.Member.Mention} no idea how, but you broke it, great....");
                return;
            }

            string[] messageArray = targetMessage.Content.Split(' ');
            Random rnd = new Random();

            for (int i = 0; i < messageArray.Length; i++) {
                char[] word = messageArray[i].ToCharArray();
                messageArray[i] = new string(word.OrderBy(x => rnd.Next()).ToArray());
            }

            await ctx.RespondAsync($"{targetMessage.Author.Mention} {String.Join(' ', messageArray)}");
        }

        public override void Register() {
            Program.CommandsNext.RegisterCommands<Scramble>();
            Logger.Log("Set up scramble command!", Logger.Level.INFO);
        }
    }
}
