using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

using Exterminator.Interfaces.Core;
using Exterminator.Modules.Core;
using DSharpPlus.Entities;

namespace Exterminator.Modules.Commands {
    public class Mock : BaseCommand {
        [Command("mock")]
        [Description("Mocks a message, sPoNgEbOb sTyLe.")]
        public async Task Command(CommandContext ctx, DiscordMember member = null) {
            await ctx.TriggerTypingAsync();

            if (member == ctx.Client.CurrentUser) {
                await ctx.RespondAsync($"{ctx.Member.Mention} I'm not going to mock myself...");
                return;
            }

            IReadOnlyList<DiscordMessage> messages;
            List<DiscordMessage> targetMessages = new List<DiscordMessage>();
            DiscordMessage targetMessage = null;

            if (member == null) {
                messages = await ctx.Channel.GetMessagesAsync(50);
            } else {
                messages = await ctx.Channel.GetMessagesAsync(100);
                foreach (DiscordMessage msg in messages) {
                    if (msg.Author.Equals(member)) {
                        targetMessages.Add(msg);
                    }
                }
            }

            if ((member != null && targetMessages.Count == 0) || messages.Count == 0) {
                await ctx.RespondAsync($"{ctx.Member.Mention} no idea how, but you broke it, great....");
                return;
            }

            if (member == null) {
                int index = 1;
                while (messages[index].Content.StartsWith("!!") || messages[index].Author == ctx.Client.CurrentUser) {
                    index++;
                }
                targetMessage = messages[index];
            } else {
                int index = 0;
                while (targetMessages[index].Content.StartsWith("!!")) {
                    index++;
                }
                targetMessage = targetMessages[index];
            }

            if (targetMessage == null) {
                await ctx.RespondAsync($"{ctx.Member.Mention} no idea how, but you broke it, great....");
                return;
            }

            char[] messageArray = targetMessage.Content.ToCharArray();

            for (int i = 0; i < messageArray.Length; i++) {
                if (messageArray[i] == ' ') {
                    continue;
                }

                if (i % 2 != 0) {
                    messageArray[i] = char.ToUpper(messageArray[i]);
                } else {
                    messageArray[i] = char.ToLower(messageArray[i]);
                }
            }

            await ctx.RespondAsync($"{targetMessage.Author.Mention} {(new string(messageArray)).Replace("@", "")}");
        }

        public override void Register() {
            Program.CommandsNext.RegisterCommands<Mock>();
            Logger.Log("Set up mock command!", Logger.Level.INFO);
        }
    }
}
