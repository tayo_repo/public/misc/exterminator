using System;
using System.Threading;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using Exterminator.Modules.Core;
using Exterminator.Modules.Core.Database;
using DSharpPlus.Entities;

namespace Exterminator.Modules.Commands {
    public class Avatar : BaseCommand {
        [Command("avatar")]
        [Description("Displays your (or someone else's) Discord avatar.")]
        public async Task Command(CommandContext ctx, DiscordUser member = null) {
            if (member == null) {
                member = ctx.User;
            }

            var embed = new DiscordEmbedBuilder();
            embed = embed.WithAuthor($"{member.Username}'s avatar", null, member.AvatarUrl);
            embed = embed.WithColor(DiscordColor.Orange);
            embed = embed.WithImageUrl(member.AvatarUrl);
            embed = embed.WithFooter("\u00a9 TAYO Creative — Built for the lulz");

            await ctx.RespondAsync(null, false, embed);
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        override public void Register() {
            Program.CommandsNext.RegisterCommands<Avatar>();
            Logger.Log("Set up avatar command!", Logger.Level.INFO);
        }
    }
}