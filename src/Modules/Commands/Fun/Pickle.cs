using System;
using System.Threading;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using Exterminator.Modules.Core;
using Exterminator.Modules.Core.Database;
using DSharpPlus.Entities;

namespace Exterminator.Modules.Commands {
    public class Pickle : BaseCommand {
        /* 
         * Implements the Command() function which is called when the
         * CommandsNext Module triggers it
         */
        [Command("pickle")]
        [Description("Shows your pickle size, and maybe changes it a bit :wink:")]
        public async Task Command(CommandContext ctx, DiscordMember member = null) {
            if (member != null && member.Id != ctx.Member.Id) {
                await ctx.RespondAsync(
                    $"{ctx.Member.Mention} WHY DO YOU WANT TO LOOK AT THEIR PICKLE HUH?!"
                );

                return;
            }

            int rick = new Random().Next(0, 50);
            if (rick == 1) {
                await ctx.RespondAsync(
                    "https://www.youtube.com/watch?v=qt0lSkhqdf8"
                );
                return;
            }

            var data = await new Command<dynamic>().Query(
                $@"SELECT id, user_id, size FROM users_pickle 
                    WHERE user_id = '{ctx.Member.Id.ToString()}'"
            );

            int size;
            bool initial = true;

            if (data.Count == 0) {
                size = new Random().Next(10, 400);
                await new Command<dynamic>().Query(
                $@"INSERT INTO users_pickle (user_id, size) VALUES 
                        ('{ctx.Member.Id.ToString()}', {size})"
                );
            } else {
                initial = false;
                size = (int)data[0]["size"];
            }

            await ctx.RespondAsync($"{ctx.Member.Mention} your pickle size is {size}mm.");

            if (size >= 150 && !initial) {
                int random = new Random().Next(5, 50);
                size -= random;
                await ctx.RespondAsync($"What is it you're looking for? Validation? You just lost {random.ToString()}mm.");
                await new Command<dynamic>().Query(
                $@"UPDATE users_pickle SET size = {size.ToString()} WHERE id = '{data[0]["id"]}'");
                return;
            }

            if (size < 50) {
                await ctx.RespondAsync("LOOOOOL THAS TINY");
                return;
            }

            if (size < 100) {
                await ctx.RespondAsync("Oh poor you, good luck with that one.");
                return;
            }

            if (size < 150) {
                await ctx.RespondAsync("Not doing too well I see.");
                return;
            }
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        override public void Register() {
            Program.CommandsNext.RegisterCommands<Pickle>();
            Logger.Log("Set up pickle command!", Logger.Level.INFO);
        }
    }
}