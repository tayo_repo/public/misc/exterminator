using System.Threading.Tasks;
using System.Collections.Generic;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

using Exterminator.Modules.Core;
using Exterminator.Modules.Core.Database.Queries;
using Exterminator.Modules.Xp;

namespace Exterminator.Modules.Commands {
    public class Rank : BaseCommand {
        [Command("rank")]
        [Description("Displays your (or someone else's) global rank.")]
        public async Task Command(CommandContext ctx, DiscordMember member = null) {
            await ctx.Channel.TriggerTypingAsync();

            DiscordEmbedBuilder embed = new DiscordEmbedBuilder();

            DiscordUser user = ctx.User;

            if (member != null) {
                user = member;
            }

            embed = embed.WithAuthor($"{user.Username}'s rank", null, user.AvatarUrl);
            embed = embed.WithColor(DiscordColor.Orange);
            embed = embed.WithThumbnailUrl(user.AvatarUrl);

            List<UsersGlobalExperience> globalExperience = await UsersGlobalExperience.FindByUser(user.Id.ToString());

            UsersGlobalExperience userExperience = null;

            if (globalExperience.Count > 0) {
                userExperience = globalExperience[0];
            }

            if (userExperience == null) {
                if (member == null) {
                    await ctx.RespondAsync($"{user.Mention} you're not ranked on the global leaderboard yet!");
                } else {
                    await ctx.RespondAsync($"{user.Username} isn't ranked on the global leaderboard yet!");
                }
            } else {
                var levelXp = Global.GetLevelXp(userExperience.level);
                var sectionSize = levelXp / 10;
                var currentProgress = userExperience.xpTowardsNext / sectionSize;

                string progressBar = "";

                for (int i = 0; i < currentProgress; i++) {
                    progressBar += "█";
                }

                while (progressBar.Length < 10) {
                    progressBar += "▓";
                }

                embed = embed.WithTitle($"Level {userExperience.level}");
                embed = embed.AddField($"{userExperience.xpTowardsNext} {progressBar} {levelXp}", $"Rank #{userExperience.rank}");

                embed = embed.WithFooter("\u00a9 TAYO Creative — Built for the lulz");

                await ctx.RespondAsync(null, false, embed);
            }
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        override public void Register() {
            Program.CommandsNext.RegisterCommands<Rank>();
            Logger.Log("Set up rank command!", Logger.Level.INFO);
        }
    }
}