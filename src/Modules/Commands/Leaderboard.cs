using System.Threading.Tasks;
using System.Collections.Generic;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

using Exterminator.Modules.Core;
using Exterminator.Modules.Core.Database.Queries;

namespace Exterminator.Modules.Commands {
    public class Leaderboard : BaseCommand {
        [Command("leaderboard")]
        [Description("Displays the global leaderboard.")]
        public async Task Command(CommandContext ctx, int page = 1) {
            if (page < 1) {
                return;
            }

            await ctx.Channel.TriggerTypingAsync();

            DiscordEmbedBuilder embed = new DiscordEmbedBuilder();

            embed = embed.WithAuthor($"Exterminator leaderboard — page {page}", null, ctx.Client.CurrentUser.AvatarUrl);
            embed = embed.WithColor(DiscordColor.Orange);

            List<UsersGlobalExperience> globalExperience = await UsersGlobalExperience.SelectLimit(10, (page - 1) * 10);

            List<Task<DiscordUser>> discordUsers = new List<Task<DiscordUser>>(globalExperience.Count);

            foreach (var user in globalExperience) {
                discordUsers.Add(ctx.Client.GetUserAsync(System.UInt64.Parse(user.userId)));
            }

            for (int i = 0; i < discordUsers.Count; i++) {
                DiscordUser _user = await discordUsers[i];
                UsersGlobalExperience _experience = globalExperience[i];

                embed = embed.AddField(_user.Username, $"#{_experience.rank} — Level {_experience.level} — Total XP {_experience.xp}");
            }

            embed = embed.WithFooter("\u00a9 TAYO Creative — Built for the lulz");

            await ctx.RespondAsync(null, false, embed);
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        override public void Register() {
            Program.CommandsNext.RegisterCommands<Leaderboard>();
            Logger.Log("Set up leaderboard command!", Logger.Level.INFO);
        }
    }
}