using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

using Exterminator.Modules.Core;

namespace Exterminator.Modules.Commands {
    public class Guilds : BaseCommand {
        /* 
         * Implements the Command() function which is called when the
         * CommandsNext Module triggers it
         */
        [Command("guilds")]
        [Description("Shows the guild count the bot is connected to.")]
        [Hidden]
        [RequireOwner]
        public async Task Command(CommandContext ctx) {

            var embed = new DiscordEmbedBuilder();
            embed = embed.WithColor(DiscordColor.Orange);
            embed = embed.WithAuthor("Connected Guild count", null, ctx.Client.CurrentUser.AvatarUrl);
            embed = embed.WithDescription($"I'm connected to {ctx.Client.Guilds.Count} guilds!");
            embed = embed.WithFooter("\u00a9 TAYO Creative — Built for the lulz");

            await ctx.RespondAsync(null, false, embed);
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        override public void Register() {
            Program.CommandsNext.RegisterCommands<Guilds>();
            Logger.Log("Set up guilds command!", Logger.Level.INFO);
        }
    }
}