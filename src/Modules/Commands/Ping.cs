using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

using Exterminator.Interfaces.Core;
using Exterminator.Modules.Core;

namespace Exterminator.Modules.Commands {
    public class Ping : BaseCommand {

        /* 
         * Implements the Command() function which is called when the
         * CommandsNext Module triggers it
         */
        [Command("ping")]
        [Description("Response with the latency between the bot and Discord.")]
        public async Task Command(CommandContext ctx) {
            await ctx.RespondAsync($"{ctx.Client.Ping}ms");
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        override public void Register() {
            Program.CommandsNext.RegisterCommands<Ping>();
            Logger.Log("Set up ping command!", Logger.Level.INFO);
        }
    }
}