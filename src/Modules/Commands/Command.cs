using System.Threading.Tasks;

using DSharpPlus.CommandsNext;

using Exterminator.Interfaces.Core;
using Exterminator.Modules.Core;

namespace Exterminator.Modules.Commands {
    public abstract class BaseCommand : ICommand {
        static public async Task onError(DSharpPlus.CommandsNext.CommandErrorEventArgs error) {
            await error.Context.RespondAsync(
                $"{error.Context.User.Mention} Something went wrong while running the {error.Command.Name} command!");
            Logger.Log($"{error.Command.Name} failed with exception below!", Logger.Level.EXEP);
            Logger.Log(error.Exception);
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        public virtual void Register() {
            throw new System.NotImplementedException();
        }
    }
}