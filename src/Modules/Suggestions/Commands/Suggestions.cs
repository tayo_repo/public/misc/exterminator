using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus;

using Exterminator.Modules.Core;
using Exterminator.Modules.Commands;
using Exterminator.Modules.Suggestions.Queries;

namespace Exterminator.Modules.Suggestions.Commands {

    [Group("suggestions")]
    public class Suggestions : BaseCommand {
        [Command("set")]
        [RequirePermissions(DSharpPlus.Permissions.Administrator)]
        public async Task Set(CommandContext ctx, DiscordChannel discordChannel = null) {
            if (discordChannel == null) {
                await ctx.RespondAsync($"{ctx.Member.Mention} I need a channel to set....");
                return;
            }

            if (discordChannel.Type != ChannelType.Text) {
                await ctx.RespondAsync($"{ctx.Member.Mention} Really? You're not giving me a text channel??");
                return;
            }

            var suggestionsChannels = await SuggestionsChannels.FindByGuild(ctx.Guild.Id.ToString());

            if (suggestionsChannels.Count > 0) {
                var channel = suggestionsChannels[0];
                channel.channelId = discordChannel.Id.ToString();

                await SuggestionsChannels.Update(channel);
            } else {
                var channel = new SuggestionsChannels(null, ctx.Guild.Id.ToString(), discordChannel.Id.ToString(), null);
                await SuggestionsChannels.Insert(channel);
            }

            await ctx.RespondAsync($"{ctx.Member.Mention} Updated the suggestions channel.");
        }

        [Command("remove")]
        [RequirePermissions(DSharpPlus.Permissions.Administrator)]
        public async Task Remove(CommandContext ctx) {
            await SuggestionsChannels.Delete(ctx.Guild.Id.ToString());

            await ctx.RespondAsync($"{ctx.Member.Mention} Removed the suggestions channel if it was there.");
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        override public void Register() {
            Program.CommandsNext.RegisterCommands<Suggestions>();
            Logger.Log("Set up suggestions module!", Logger.Level.INFO);
        }
    }
}