
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Exterminator.Modules.Core.Database.Queries;
using Exterminator.Modules.Core;
using System.Globalization;
using System;

namespace Exterminator.Modules.Xp {
    abstract class Generic {
        static public async Task OnMessage(DSharpPlus.EventArgs.MessageCreateEventArgs msg) {
            if (msg.Author.IsBot == true || msg.Guild == null) {
                return;
            }

            try {
                int sentMessageIndex = 0;
                int firstNonBotMessageIndex = 0;

                bool isMessageValid = isValid(msg.Message.Content);
                bool lastMessageBySameAuthor = false;

                IReadOnlyList<DSharpPlus.Entities.DiscordMessage> lastChannelMessages = null;
                try {
                    lastChannelMessages = await msg.Channel.GetMessagesAsync(5);
                } catch (System.Exception) {
                    // if we can't get the data, no XP fam
                    return;
                }

                if (lastChannelMessages != null && lastChannelMessages.Count != 0) {
                    while (sentMessageIndex + 1 < lastChannelMessages.Count && lastChannelMessages[sentMessageIndex].Id != msg.Message.Id) {
                        sentMessageIndex += 1;
                        firstNonBotMessageIndex += 1;
                    }

                    while (firstNonBotMessageIndex + 1 < lastChannelMessages.Count &&
                        (lastChannelMessages[firstNonBotMessageIndex].Author.IsBot == true
                            || lastChannelMessages[sentMessageIndex].Id == lastChannelMessages[firstNonBotMessageIndex].Id)) {
                        firstNonBotMessageIndex += 1;
                    }

                    if (sentMessageIndex != firstNonBotMessageIndex) {
                        if (lastChannelMessages[sentMessageIndex].Author.Id == lastChannelMessages[firstNonBotMessageIndex].Author.Id) {
                            lastMessageBySameAuthor = true;
                        }
                    }
                }

                List<UsersMessageExperience> messages = await UsersMessageExperience.FindByUser(msg.Author.Id.ToString());

                if (messages.Count == 0) {

                    UsersMessageExperience data = new UsersMessageExperience(null, msg.Author.Id.ToString(),
                        msg.Guild.Id.ToString(), 0, new Random().Next(5, 16), null, null, msg.Message.Content);

                    if (isMessageValid == true) {
                        data.messageCount += 1;
                    }

                    await UsersMessageExperience.Insert(data);
                } else {
                    UsersMessageExperience guildMessage = messages.Find(message => message.guildId == msg.Guild.Id.ToString());

                    if (guildMessage == null) {
                        UsersMessageExperience data = new UsersMessageExperience(null, msg.Author.Id.ToString(),
                            msg.Guild.Id.ToString(), 0, new Random().Next(5, 16), null, null, msg.Message.Content);

                        if (isMessageValid == true) {
                            data.messageCount += 1;
                        }

                        await UsersMessageExperience.Insert(data);
                    } else {
                        UsersMessageExperience data = guildMessage;
                        if (DateTime.Now.Subtract((DateTime)data.creationDate).Minutes > 4) {
                            await Server.AddXp(msg, data.messageCount * data.randomGainFactor);
                            await Global.AddXp(msg, data.messageCount * data.randomGainFactor);

                            data.creationDate = DateTime.Now;

                            data.messageCount = 0;
                            data.randomGainFactor = new Random().Next(5, 16);
                        }

                        if (isMessageValid == true && lastMessageBySameAuthor == false) {
                            data.messageCount += 1;
                            data.lastGainDate = DateTime.Now;
                        }

                        data.lastMessage = msg.Message.Content;

                        await UsersMessageExperience.Update(data);
                    }
                }
            } catch (System.Exception exception) {
                Logger.Log(exception);
            }
        }

        private static string removeInvalid(string message) {
            // Remove mentions, channels, and emoji
            message = new Regex(@"<@!?(\d+)>", RegexOptions.ECMAScript).Replace(message, "");
            message = new Regex(@"<@&(\d+)>", RegexOptions.ECMAScript).Replace(message, "");
            message = new Regex(@"<#(\d+)>", RegexOptions.ECMAScript).Replace(message, "");
            message = new Regex(@"<:([a-zA-Z0-9_]+):(\d+)>", RegexOptions.ECMAScript).Replace(message, "");

            // Clear of spaces
            message = message.Replace(" ", "");

            // Split the message in lines
            string[] messageLines = message.Split('\n');

            // New message to start fresh
            string newMessage = "";

            // Remove quotes from the message
            foreach (var line in messageLines) {
                if (line.StartsWith('>') == false) {
                    newMessage += line;
                }
            }

            // Remove markup from the message
            newMessage = newMessage.Replace("|", "");
            newMessage = newMessage.Replace("`", "");
            newMessage = newMessage.Replace("*", "");
            newMessage = newMessage.Replace("_", "");
            newMessage = newMessage.Replace("~", "");
            return newMessage;
        }

        private static bool isValid(string message, UsersMessageExperience lastMessage = null) {
            message = removeInvalid(message);

            if (message.Length < 10) {
                return false;
            }

            if (lastMessage != null) {
                if (lastMessage.lastGainDate != null) {
                    if (DateTime.Now.Subtract((DateTime)lastMessage.lastGainDate).Seconds < 6) {
                        return false;
                    }
                }

                if (message.Equals(lastMessage.lastMessage)) {
                    return false;
                }
            }

            return true;
        }
    }
}