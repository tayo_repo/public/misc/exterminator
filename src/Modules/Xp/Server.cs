using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

using Exterminator.Modules.Core.Database.Queries;
using Exterminator.Modules.Core;

namespace Exterminator.Modules.Xp {
    abstract class Server {
        static public async Task AddXp(DSharpPlus.EventArgs.MessageCreateEventArgs msg, long xp) {
            try {
                List<UsersServerExperience> messages =
                    await UsersServerExperience.FindByUser(msg.Author.Id.ToString(), msg.Guild.Id.ToString());

                long oldLevel = 0;
                long newLevel = 0;
                long originalXpGain = xp;
                long levelXp = GetLevelXp(newLevel);

                if (messages.Count == 0) {
                    while (levelXp < xp) {
                        xp -= levelXp;
                        newLevel += 1;
                        levelXp = GetLevelXp(newLevel);
                    }

                    UsersServerExperience data = new UsersServerExperience(null, msg.Author.Id.ToString(),
                        msg.Guild.Id.ToString(), originalXpGain, xp, newLevel, null);

                    await UsersServerExperience.Insert(data);
                } else {
                    UsersServerExperience message = messages.Find(message => message.guildId == msg.Guild.Id.ToString());

                    if (message == null) {
                        while (levelXp < xp) {
                            xp -= levelXp;
                            newLevel += 1;
                            levelXp = GetLevelXp(newLevel);
                        }

                        UsersServerExperience data = new UsersServerExperience(null, msg.Author.Id.ToString(),
                            msg.Guild.Id.ToString(), originalXpGain, xp, newLevel, null);

                        await UsersServerExperience.Insert(data);
                    } else {
                        UsersServerExperience data = message;

                        oldLevel = data.level;
                        newLevel = data.level;
                        levelXp = GetLevelXp(oldLevel);
                        data.xp += originalXpGain;
                        data.xpTowardsNext += originalXpGain;

                        while (levelXp < data.xpTowardsNext) {
                            data.xpTowardsNext -= levelXp;
                            newLevel += 1;
                            levelXp = GetLevelXp(newLevel);
                        }

                        data.level = newLevel;

                        await UsersServerExperience.Update(data);
                    }
                }

                if (oldLevel != newLevel) {
                    List<ExperienceRoleRewards> roles =
                        await ExperienceRoleRewards.FindByGuild(newLevel, msg.Guild.Id.ToString());

                    var member = msg.Guild.GetMemberAsync(msg.Author.Id);

                    if (roles.Count != 0 && msg.Guild.Roles.Count != 0) {
                        foreach (var role in roles) {
                            DSharpPlus.Entities.DiscordRole discordRole = null;

                            try {
                                discordRole = msg.Guild.Roles.First(x => x.Id.ToString() == role.roleId);
                            } catch (System.Exception) {
                                await ExperienceRoleRewards.Delete(role.guildId, role.roleId);
                                continue;
                            }

                            await msg.Guild.GrantRoleAsync(await member, discordRole);
                        }
                    } else {
                        if (roles.Count != 0) {
                            foreach (var role in roles) {
                                await ExperienceRoleRewards.Delete(role.guildId, role.roleId);
                            }
                        }
                    }
                }
            } catch (System.Exception exception) {
                Logger.Log(exception);
            }
        }

        static public long GetLevelXp(long level) {
            return (250 * level) + 250;
        }
    }
}