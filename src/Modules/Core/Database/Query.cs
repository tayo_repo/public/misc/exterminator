using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Npgsql;

using Exterminator.Interfaces.Core.Database;

namespace Exterminator.Modules.Core.Database {
    public class Command<T> : IQuery<T> {
        public async Task<List<T>> Query(string query) {
            List<T> resultset = new List<T>();

            using (NpgsqlCommand cmd = new NpgsqlCommand(query, Connection.Get()))
            using (System.Data.Common.DbDataReader reader = await cmd.ExecuteReaderAsync()) {
                while (reader.Read()) {
                    dynamic entry = new Dictionary<string, dynamic>();
                    for (int i = 0; i < reader.FieldCount; i++) {
                        Type type = reader.GetFieldType(i);
                        entry[reader.GetName(i)] = Convert.ChangeType(reader.GetFieldValue<Object>(i), type);
                    }
                    resultset.Add(entry);
                }
                return resultset;
            }
        }
    }
}
