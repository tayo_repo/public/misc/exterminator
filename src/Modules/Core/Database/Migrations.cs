using System;
using System.IO;
using System.Threading.Tasks;
using Npgsql;

namespace Exterminator.Modules.Core.Database {
    public static class Migrations {
        public static async Task Run() {
            bool initialSchema = true;
            int migrationsRan = 0;

            var data = await new Command<dynamic>().Query(
                $@"SHOW TABLES;"
            );

            for (int i = 0; i < data.Count; i++) {
                if (data[i]["table_name"] == "exterminator_schema_version") {
                    initialSchema = false;
                }
            }

            if (initialSchema == true) {
                await new Command<dynamic>().Query(
                    $@"CREATE TABLE exterminator_schema_version (
                        id UUID NOT NULL DEFAULT gen_random_uuid(),
                        version INT,
                        PRIMARY KEY (id, version)
                    );"
                );
                await new Command<dynamic>().Query(
                    $@"INSERT INTO exterminator_schema_version (version) VALUES 
                        (0)"
                );
            }

            var version = await new Command<dynamic>().Query(
                $@"SELECT id, version FROM exterminator_schema_version;"
            );

            int schemaVersion = (int)version[0]["version"];
            Guid versionId = version[0]["id"];

            while (File.Exists($"./Migrations/{schemaVersion + 1}.sql")) {
                string sql = File.ReadAllText($"./Migrations/{schemaVersion + 1}.sql");

                // Process SQL migration
                using (NpgsqlCommand cmd = new NpgsqlCommand(sql, Connection.Get())) {
                    cmd.ExecuteNonQuery();
                }

                schemaVersion++;
                migrationsRan++;
            }

            Logger.Log($"Ran {migrationsRan.ToString()} migration(s)", Logger.Level.VERB);

            await new Command<dynamic>().Query(
                $@"UPDATE Exterminator_Schema_Version SET version = {schemaVersion.ToString()}
                    WHERE id = '{versionId.ToString()}'"
            );

            Logger.Log($"Updated schema to version {schemaVersion.ToString()}", Logger.Level.VERB);
        }
    }
}