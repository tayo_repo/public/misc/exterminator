using System.Threading.Tasks;
using DSharpPlus.CommandsNext;

namespace Exterminator.Interfaces.Core {
    public interface ICommand {
        void Register();
    }
}