using System.Collections.Generic;
using System.Threading.Tasks;

namespace Exterminator.Interfaces.Core.Database {
    public interface IQuery<T> {
        Task<List<T>> Query(string query);
    }
}
