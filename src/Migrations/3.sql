CREATE TABLE experience_role_rewards (
    id UUID NOT NULL DEFAULT gen_random_uuid(),
    guild_id TEXT,
    role_id TEXT,
    level INT NOT NULL DEFAULT 0,
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);