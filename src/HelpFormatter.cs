using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Converters;
using DSharpPlus.CommandsNext.Entities;
using DSharpPlus.Entities;

namespace Exterminator {
    public class HelpFormatter : IHelpFormatter {
        private StringBuilder MessageBuilder { get; }

        public HelpFormatter() {
            this.MessageBuilder = new StringBuilder();
        }

        public IHelpFormatter WithCommandName(string name) {
            this.MessageBuilder.AppendLine(Formatter.Bold("Command"))
                .AppendLine(name).AppendLine();

            return this;
        }

        public IHelpFormatter WithDescription(string description) {
            if (string.IsNullOrEmpty(description) == true) {
                description = "No description has been provided for this command.";
            }

            this.MessageBuilder.AppendLine(Formatter.Bold("Description"))
                .AppendLine(Formatter.Italic(description))
                .AppendLine();

            return this;
        }

        public IHelpFormatter WithGroupExecutable() {
            this.MessageBuilder.AppendLine(Formatter.Bold("Note"))
                .AppendLine(Formatter.Italic("This command can be ran without subcommands."))
                .AppendLine();

            return this;
        }

        public IHelpFormatter WithAliases(IEnumerable<string> aliases) {
            this.MessageBuilder.Append(Formatter.Bold("Aliases")).Append('\n');

            foreach (var alias in aliases) {
                this.MessageBuilder.AppendLine($"- {alias}");
            }

            this.MessageBuilder.AppendLine();


            return this;
        }

        public IHelpFormatter WithArguments(IEnumerable<CommandArgument> arguments) {
            this.MessageBuilder.Append(Formatter.Bold("Arguments")).Append('\n');

            foreach (var argument in arguments) {
                this.MessageBuilder.AppendLine($"- {argument.Name} ({Formatter.Italic(argument.Type.ToUserFriendlyName())})");
            }

            this.MessageBuilder.AppendLine();

            return this;
        }

        public IHelpFormatter WithSubcommands(IEnumerable<Command> subcommands) {
            this.MessageBuilder.Append(Formatter.Bold("Subcommands")).Append('\n');

            foreach (var subcommand in subcommands) {
                this.MessageBuilder.AppendLine($"- {subcommand.Name}");
            }

            this.MessageBuilder.AppendLine();

            return this;
        }

        // this is called as the last method, this should produce the final 
        // message, and return it
        public CommandHelpMessage Build() {
            var embed = new DiscordEmbedBuilder();
            embed = embed.WithColor(DiscordColor.Orange);
            embed = embed.WithTitle("Help");
            embed = embed.WithFooter("\u00a9 TAYO Creative — Built for the lulz");
            embed = embed.WithDescription(this.MessageBuilder.ToString().Replace("\r\n", "\n"));

            return new CommandHelpMessage(null, embed);
        }
    }
}
