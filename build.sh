
if [ -f "./out/config.json" ]; then
    mv ./out/config.json ./config.json
fi

# Remove out folder
rm -rf ./out/*

# Build project
dotnet publish --runtime ubuntu.16.04-x64 -c Release -o out
cp -r ./src/AnimeFaceDetection ./out/AnimeFaceDetection
cp -r ./src/Migrations ./out/Migrations
cd ./out/AnimeFaceDetection
make clean
make

cd ../../

if [ -f "./config.json" ]; then
    mv ./config.json ./out/config.json
fi

# TODO: add multi-platform builds